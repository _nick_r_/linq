﻿using LINQ.Models;
using LINQ.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            LINQService linqService = new LINQService();
            var projects = HTTPService.GetTasks().Result;


            // testing
            linqService.GetTasks(0);
            Console.ReadKey();
            Console.Clear();

            linqService.GetTasksForPerformer(23);
            Console.ReadKey();
            Console.Clear();


            linqService.GetFinishedTasks(22);
            Console.ReadKey();
            Console.Clear();

            linqService.GetElderUsers();
            Console.ReadKey();
            Console.Clear();

            linqService.GetSortedUsersWithTasks();
            Console.ReadKey();
            Console.Clear();

            linqService.GetStructureSix(0);
            Console.ReadKey();
            Console.Clear();

            linqService.GetStructureSeven(1);

            Console.ReadKey();
        }
    }
}
